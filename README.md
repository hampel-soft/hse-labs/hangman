# Hangman

Just a little Hangman game implemented in LabVIEW with DQMH and the HSE Application Template.


## :bulb: Usage

Run the `startup.vi` from the LabVIEW project file in `\hangman\Hangman_Source`.

The words to guess are defined in the `hangman-words.txt` file in the `\hangman\Hangman_Config\Unit_SomeUnitId` directory.


## :rocket: Installation

### :wrench: LabVIEW 2020

The VIs are maintained in LabVIEW 2020.

### :link: Dependencies

This demo application depends on :
* https://dokuwiki.hampel-soft.com/code/dqmh/hse-application-template
* https://dokuwiki.hampel-soft.com/code/open-source/hse-libraries
* https://dokuwiki.hampel-soft.com/code/open-source/hse-db


## :busts_in_silhouette: Contributing 

We welcome every and any contribution. On our Dokuwiki, we compiled detailed information on 
[how to contribute](https://dokuwiki.hampel-soft.com/processes/collaboration). 
Please get in touch at (office@hampel-soft.com) for any questions.


##  :beers: Credits

* Coding: Joerg Hampel
* Scarecrow image: https://pixabay.com/illustrations/scarecrow-halloween-scary-pumpkin-5617119/
* Gallows background: https://pixabay.com/photos/winter-s-gibbet-elsdon-3763870/


## :page_facing_up: License 

This project is licensed under a modified BSD License - see the [LICENSE](LICENSE) file for details.
